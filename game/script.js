function enemy(x, y, moveId = 0) {
    this.x = x;
    this.y = y;
    this.moveId = moveId;
    this.tX;
    this.tY;

}

let map = [600, 600];


let road = {
    'start': [100, 100],
    'paths': [
        [250, 250],
        [500, 250],
        [500, 450],
        [200, 450]
    ]
};

function createRoad() {
    //рисуем дорогу, обертку svg
    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttributeNS(null, "viewBox", "0 0 " + map[0] + " " + map[1]);
    svg.setAttributeNS(null, "width", map[0]);
    svg.setAttributeNS(null, "height", map[1]);
    svg.setAttributeNS(null, "stroke", 'blue');
    //рисуем все пути дороги, по координатам
    //todo переделать на объекты, дорог может быть несколько
    let start = road.start;
    for (i = 0; i < road.paths.length; i++) {
        var line = document.createElementNS('http://www.w3.org/2000/svg', "line");
        let path = road.paths[i];
        line.style.stroke = "grey";
        line.style.strokeWidth = "10px";
        line.setAttribute('x1', start[0]);
        line.setAttribute('y1', start[1]);
        line.setAttribute('x2', path[0]);
        line.setAttribute('y2', path[1]);
        line.setAttribute("stroke", "black")
        svg.appendChild(line);
        start = path;
        world.append(svg);
    }

}

var enemy = new enemy(100, 100);

let world = document.getElementById("world");

function createEnemies(x, y) {
    for (let i = 0; i < 1; i++) {

        // Создание прямоугольника и придание ему стилей

        let newElement = document.createElement("div");
        newElement.className = "enemy";
        newElement.id = "enemy" + i;
        newElement.style.top = x + "px";
        newElement.style.left = y + "px";

        // Вставка прямоугольника в world

        world.append(newElement);
    }
}

function update() {

    console.log('enemy.moveId:' + enemy.moveId + ' x: ' + enemy.x + ", y: " + enemy.y);
    if (enemy.x >= enemy.tX && enemy.y >= enemy.tY) {
        clearInterval(enemy.moveId);
    }
}

function moveLeftDown(x, y) {

    element = document.getElementById("enemy0");
    enemy.x += 0.1;
    enemy.y += 0.1;
    element.style.top = enemy.x + "px";
    element.style.left = enemy.y + "px";
    enemy.tX = x;
    enemy.tY = y;
}

function move(x, y) {
    enemy.moveId = setInterval(moveLeftDown, 10, x, y);

}


createRoad();
createEnemies(100, 100);
move(250, 250);

let TimerGame = setInterval(update, 10);


